# Simple module demonstrating adding unlimited fields to a form

This is a simple Drupal 8 module demonstrating how to programmatically 
create a form that allows the user to add unlimited fields to their
submission.

## Installation

Place this in your modules folder with the name `demo_form_unlimited_values` 
(eg, `/modules/custom/demo_form_unlimited_values/`).